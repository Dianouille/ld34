int time;
int timer = 3000;
int timeMenu;
String screenName;
String GAME_VIEW = "gameView";
String MENU_VIEW = "menuView";
String WIN_VIEW = "winView";
String LOSE_VIEW ="loseView";
int bX1;
int bY1;
int bWidth;
int bHeight;
int bgX;
int bgY;
PFont current;
int life=5;


void setup (){
  //println("####---- setup");
//fullScreen();
size(1280, 800);
background(255);
prepareCards();
time=millis();
screenName = MENU_VIEW;
//PImage bg;
//bg=loadImage("Phaseolus_Background.png");
  smooth();
  frameRate(60);
}
ArrayList<PImage> Cards =new ArrayList<PImage>();
int random1 = 0;
int random2 = 0; 
PImage card1;
PImage card2;
int score = 0;
ArrayList<PImage> flowers = new ArrayList<PImage>();
PImage heart;
PImage bg;
PImage instruction;

void prepareCards (){
 heart = loadImage("Life.png");
 bg=loadImage("Phaseolus_Background.png");
 instruction=loadImage("Instructions.png");
//println("####---- prepareCards");
Cards.add(loadImage("Image1.png"));
Cards.add(loadImage("Image2.png"));
Cards.add(loadImage("Image3.png"));
Cards.add(loadImage("Image4.png"));
Cards.add(loadImage("Image5.png"));
Cards.add(loadImage("Image6.png"));
  for(int i=0; i<25; i++){
    flowers.add(loadImage("Flower"+ i+".png"));
  }
}

void startGame(){
  //println("####---- StartGame");
  println(score);
  random1= (int)random(0, (Cards.size()));
  random2= (int)random(0, (Cards.size()));
  card1= Cards.get(random1);
  card2= Cards.get(random2);
  time=millis();
  timer=max(4000-((millis()-timeMenu)/30),500);
  //println("valeur du timer"+timer);
}

boolean checkResult(){
  if (random1 == random2){
    return true;
  }
  else{
    return false;
  }
}

void drawGame(){
      background(0);
      bgX = 0;
      if(score>=52){
      bgY=(height-(6000-((score-52)*100)));
      }
      else{
        bgY=height-6000;
      }
      tint(255, 255);
      image(bg, bgX, bgY, width, 6000);
      PImage flower = flowers.get(max(score/4, 0));
      image(flower, (width/2)-flower.width/2, 10);
    if(card1 != null){
      //println(((float)millis()-time)/(float)timer);
      //println((float)timer);
      float timeRatio = ((float)millis()-time)/(float)timer;
      int tempAlpha = (int)((1-timeRatio)*255);
      tint(255, tempAlpha);
      fill(100, 100, 100, 50);
      stroke(255, 70);
      strokeWeight(3);
      rect(width/2-189, height-card1.height-171, 150, 150, 10, 10, 10, 10);
      rect(width/2+39, height-card1.height-171, 150, 150, 10, 10, 10, 10);
      image(card1, (width/2-card1.width)-50, (height-card1.height)-160);
      image(card2, (width/2-card2.width)+card1.width+50, (height-card2.height)-160);
      tint(255, 255);
      
      for (int i=0; i<life; i++){
        image(heart, (((width-(life*heart.width))/2)+i*heart.width), (height-heart.height)-50) ;
      }
      
    }
    //println(millis()+"/"+time+"/"+timer+"/"+timeMenu);
    if (millis()-time>=timer){
      //println("timer marche");
      modifyScore(-1);
      startGame();
    }
    fill(0);
    //text(score, 300,300);
    if (score==100){
      screenName=WIN_VIEW;
    }
    if (life==0){
      screenName=LOSE_VIEW;
    }
}


  
  void drawMenu(){
  background(150,155,155);
    fill (26, 128, 94, 80);
    strokeWeight(1);
    stroke(255, 80);
    bX1=(width/2)-125;
    bY1=(height/2)-25;
    bWidth=250;
    bHeight=50;
    score =0;
    life=5;
  if(mousePressed){
    if((pmouseX>=bX1 && pmouseX<=bX1+bWidth && pmouseY>=bY1 && pmouseY<=bY1+bHeight)&& screenName==MENU_VIEW){ 
      timeMenu=millis();
      startGame();
      screenName=GAME_VIEW;
      fill(255, 50);
  }
  }
  PImage logo;
  logo = loadImage("Phaseolus.png");
  image(logo, ((width/2)-(468/2)), 50);
  rect(bX1, bY1, bWidth, bHeight, 15, 15, 15, 15);
  fill(255, 80);
  current = createFont("Avenir.ttc", 24);
  textFont(current);
  text("START GAME", bX1+47, bY1+35);
  image(instruction, (width-instruction.width)/2, height-(instruction.height)-100);

}

  void drawWin(){
  background(50,55,55);
    fill (26, 128, 94, 80);
    strokeWeight(1);
    stroke(255, 80);
    bX1=(width/2)-125;
    bY1=(height/2)-25;
    bWidth=250;
    bHeight=50;
    score =0;
    life=5;
  if(mousePressed){
    if((pmouseX>=bX1 && pmouseX<=bX1+bWidth && pmouseY>=bY1 && pmouseY<=bY1+bHeight)&& screenName==WIN_VIEW){ 
      timeMenu=millis();
      startGame();
      screenName=GAME_VIEW;
      fill(255, 50);
  }
  }
  PImage congrat;
  congrat = loadImage("congrat.png");
  image(congrat, ((width/2)-(512/2)), 50);
  rect(bX1, bY1, bWidth, bHeight, 15, 15, 15, 15);
  fill(255, 80);
  current = createFont("Avenir.ttc", 24);
  textFont(current);
  text("RESTART GAME", bX1+47, bY1+35);
}

  void drawLose(){
  background(50,55,55);
    fill (26, 128, 94, 80);
    strokeWeight(1);
    stroke(255, 80);
    bX1=(width/2)-125;
    bY1=(height/2)-25;
    bWidth=250;
    bHeight=50;
    score =0;
    life=5;
  if(mousePressed){
    if((pmouseX>=bX1 && pmouseX<=bX1+bWidth && pmouseY>=bY1 && pmouseY<=bY1+bHeight)&& screenName==LOSE_VIEW){ 
      timeMenu=millis();
      startGame();
      screenName=GAME_VIEW;
      fill(255, 50);
  }
  }
  PImage oops;
  oops = loadImage("oops.png");
  image(oops, ((width/2)-(512/2)), 50);
  rect(bX1, bY1, bWidth, bHeight, 15, 15, 15, 15);
  fill(255, 80);
  current = createFont("Avenir.ttc", 24);
  textFont(current);
  text("RESTART GAME", bX1+47, bY1+35);
}

  
void draw(){
  if(screenName == GAME_VIEW){
    drawGame();
  }else if(screenName == MENU_VIEW){
    drawMenu();
  }else if(screenName == WIN_VIEW){
    drawWin();
  }else if(screenName == LOSE_VIEW){
    drawLose();
  }
}

void keyPressed(){
  if ((keyCode == RIGHT || keyCode==LEFT)&& screenName == GAME_VIEW){
    if (keyCode == RIGHT){
      if(checkResult()==true){
        modifyScore(+1);
      }
      else{
        life=life-1;
      }
    }
    if(keyCode == LEFT){
      if(checkResult()==false){
        modifyScore(+1);
      }
      else{
        life=life-1;
      }
    }
    //println(life);
    startGame();
  }
}

void modifyScore (int modifiers){
  score+=modifiers; 
}